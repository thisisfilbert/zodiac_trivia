# ZODIAC TRIVIA

This program is about generating some trivia about Western and Chinese Zodiac.

## Note

This program is under development. Because of limited knowledge of mine, currently there are moments when the program cannot decide what Chinese zodiac a person with their birth month between January and February has.

## Screenshots

[<img src=https://codeberg.org/filbertsalim/zodiac_trivia/raw/branch/main/images/Screenshot%20from%202023-02-01%2019-22-43.png/>](https://codeberg.org/filbertsalim/zodiac_trivia/raw/branch/main/images/Screenshot%20from%202023-02-01%2019-22-43.png)

## License

[MIT](https://choosealicense.com/licenses/mit/)
