print("""
ZODIAC TRIVIA
-------------
Reference: 
- https://ytliu0.github.io/ChineseCalendar/sexagenary.html for Sexagenary Cycle (六十干支)
""") 

while True:
	month_list = ("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")
	month_amount = (0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365)
	leap_year = False
	try:
		not_confirmed = True
		while not_confirmed:
			what_year = int(input("Input birth year: "))
			if (what_year % 4 == 0 and what_year % 100 != 0) or (what_year % 100 == 0 and what_year % 400 == 0):
				leap_year = True
			valid_month = False
			while not valid_month:
				what_month = int(input("Input birth month (in number, e.g.:1 for January): "))
				if what_month <= 12:
					valid_month = True
				else:
					print("")
					print("Please insert number from 1 to 12!")
			valid_date = False
			while not valid_date:
				what_date = int(input("Input birth date: "))
				total_day = what_date + month_amount[what_month - 1]
				if what_month == 2:
					if leap_year == True:
						if what_date <= 29:
							valid_date = True
						else:
							print("February has 29 days in a leap year.")
					else:
						if what_date <= 28:
							valid_date = True
						else:
							print("There is only 28 days in February.")
				elif (what_month % 2 == 0 and what_month < 8) or (what_month % 2 == 1 and what_month >= 8):
					if what_date <= 30:
						valid_date = True
					else:
						print(f"{month_list[what_month - 1]} only has 30 days.")
				elif (what_month % 2 == 1 and what_month < 8) or (what_month % 2 == 0 and what_month >= 8):
					if what_date <= 31:
						valid_date = True
					else:
						print(f"{month_list[what_month - 1]} only has 31 days.")
				print("")
				

			confirmation = input(f"Please confirm that you input '{what_date} {month_list[what_month - 1]} {what_year}' (y/n): ")
			if confirmation == "y" or confirmation == "":
				not_confirmed = False
			elif confirmation == "n":
				continue
			else:
				print("Please input 'y' or 'n'!")
			print("")
		
#Note:
#1. Zodiac elements and heavenly stems below are not listed from the first order. The order here is 7th-10th, 1st-6th.
#2. List of chinese zodiacs and earthly branches below are not listed from the first order. The order here is 9th-12th, 1st-8th.			
		
		identify_zodiac = int(what_year)
		zodiac_element = ("Metal (Gold)", "Metal (Gold)", "Water", "Water", "Wood", "Wood", "Fire", "Fire",  "Earth", "Earth")
		identify_element = identify_zodiac
		zodiac_tuple = ("Monkey", "Rooster", "Dog", "Pig", "Rat", "Ox", "Tiger", "Rabbit", "Dragon", "Snake", "Horse", "Goat")
		heavenly_stems = ("庚", "辛", "壬", "癸", "甲", "乙", "丙", "丁", "戊", "己") 
		heavenly_stems_pinyin = ("gēng", "xīn", "rén", "guǐ", "jiǎ", "yǐ", "bǐng", "dīng", "wù", "jǐ")
		earthly_branches = ("申", "酉", "戌", "亥", "子", "丑", "寅", "卯", "辰", "巳", "午", "未") 
		earthly_branches_pinyin = ("shēn", "yǒu", "xū", "hài", "zǐ", "chǒu", "yín", "mǎo", "chén", "sì", "wǔ", "wèi") 
		
		while identify_zodiac >= 12:
			identify_zodiac = identify_zodiac % 12
		while identify_element >= 10:
			identify_element = identify_element % 10
		print(f"What you may want to know about {what_date} {month_list[what_month - 1]} {what_year}: ")
		print(f"- {what_year} is the year of {zodiac_element[identify_element]} {zodiac_tuple[identify_zodiac]}.")
		if total_day < 21:
			print(f"  However, your Chinese zodiac is {zodiac_element[identify_element - 1]} {zodiac_tuple[identify_zodiac - 1]}.")
		elif total_day >= 21 and total_day <= 52:
			print(f"  It is a shame that we are not able to know what your Chinese zodiac is yet.")
		print(f"- The Sexagenary Cycle for year {what_year} is {heavenly_stems[identify_element]}{earthly_branches[identify_zodiac]} ({heavenly_stems_pinyin[identify_element]}{earthly_branches_pinyin[identify_zodiac]}).")
		
		western_zodiac_tuple = ("Capricorn", "Sagitaurus", "Scorpio", "Libra", "Virgo", "Leo", "Cancer", "Gemini", "Taurus", "Aries", "Pisces", "Aquarius")
		
		if leap_year == True:
			total_day += 1  #see total_day formula on line 29
			if what_month - 1 == 1:
				total_day -= 1
		if total_day > 354 or total_day <= 19:
			western_zodiac = western_zodiac_tuple[0]
		elif total_day > 326:
			western_zodiac = western_zodiac_tuple[1]
		elif total_day > 295:
			western_zodiac = western_zodiac_tuple[2]			
		elif total_day > 265:
			western_zodiac = western_zodiac_tuple[3]
		elif total_day > 233:
			western_zodiac = western_zodiac_tuple[4]
		elif total_day > 201:
			western_zodiac = western_zodiac_tuple[5]
		elif total_day > 171:
			western_zodiac = western_zodiac_tuple[6]
		elif total_day > 140:
			western_zodiac = western_zodiac_tuple[7]
		elif total_day > 110:
			western_zodiac = western_zodiac_tuple[8]
		elif total_day > 80:
			western_zodiac = western_zodiac_tuple[9]
		elif total_day > 49:
			western_zodiac = western_zodiac_tuple[10]
		elif total_day > 19:
			western_zodiac = western_zodiac_tuple[11]
		print(f"- The western zodiac is {western_zodiac}.")
		
		
		reask = True
		while reask:
			print("")
			confirm_to_continue = input("Do you wish to continue (y=default/n)? ")
			if confirm_to_continue == "" or confirm_to_continue == "y":
				reask = False
			elif confirm_to_continue == "n":
				exit("The 'Zodiac Trivia' has stopped.")
			else:
				print("Please hit with 'y' or 'n'!")
			
	except ValueError:
		print("Please input a number!")
	except KeyboardInterrupt:
		break
	print("")
